import React, { Component } from 'react';
import './App.css';
import '../components/Persons/Person/Person.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';

class App extends Component {

  // In the background sets the state in constructor without us needing to call it.
  state = {
    persons: [
      { id: 1, name: 'Laura', age: 25 },
      { id: 2, name: 'Conor', age: 24 }
    ],
    showPersons: false
  };

  /**
   * How to enter text data to a component
   */
  enterNameHandler = (event, id) => {

    const personIndex = this.state.persons.findIndex(persn => {
      return persn.id === id;
    });

    const person = { ...this.state.persons[personIndex] };
    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;


    this.setState({ persons: persons });
  }

  // Conditional Clicks
  togglePersonHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({ showPersons: !doesShow });
  }

  /**
   * This method is actually mutating the state.
   * To avoid this, use the slice() function.
   */
  deletePersonHandler = (index) => {
    // const persons = this.state.persons; BAD!
    const persons = this.state.persons.slice();
    persons.splice(index, 1);
    this.setState({ persons: persons });
  }


  // Ternerary operators are good but could cause performmance issues when dealing with big data.

  render() {
    // Anything within the render method is javascript
    // We can write any thing we want.

    let persons = null;
    if (this.state.showPersons) {
      persons = (
        // List array items in React using map.
        //Curly braces to be be writing in js.
        <Persons
          persons={this.state.persons}
          clicked={this.deletePersonHandler}
          changed={this.enterNameHandler} />
      );
    }

    return (
      <div className="App">
        <Cockpit toggle={this.togglePersonHandler} />
        {persons}

      </div>
    );

    /**
    return (
<div className="App">
      <h1> My people </h1>
      <button onClick={this.togglePersonHandler}>Show People</button>
      {this.state.showPersons === true ?
        <div>
          <Person
            name={this.state.person[0].name}
            age={this.state.person[0].age}
            changed={this.enterNameHandler}></Person>

          <Person name={this.state.person[1].name} age={this.state.person[1].age}></Person>
        </div> : null
      }
    </div>
    );
    */
  }
}

export default App;
