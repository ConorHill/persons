import React from 'react';
import './Person.css';

const person = (props) => {
    return (
        <div className="Person-card">
            <input type='input' onChange={props.changed} value={props.name}></input>
            <p onClick={props.click}> My name is {props.name} and I'm {props.age} years old. </p>
        </div>
    );
};

export default person;
