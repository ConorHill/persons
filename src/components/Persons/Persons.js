import React from 'react';
import Person from './Person/Person';


// Because props.persons is on the same line I can omit the return statement
// It is the return statement!
const persons = (props) => props.persons.map((person, index) => {
        // If my function is taking an input, must use the arrow function method.
        return <Person
            click={() => props.clicked(index)}
            changed={(event) => props.changed(event, person.id)}
            name={person.name}
            age={person.age}
            key={person.id} />
    });


export default persons;